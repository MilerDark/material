import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, pipe } from 'rxjs';
import { filter, take, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  url = 'https://randomuser.me/api/';
  constructor(public http: HttpClient) { }


  obtenerUsuario():Observable<any>{
    return this.http.get<any>(this.url)
  }

  obtenerUsuarioMasculino():Observable<any>{
    return this.http.get<any>(this.url).
            pipe(
              filter(
                usuario => usuario['results'][0].gender!= 'male'
              )
            );
  }

  obtenerPaisCanada():Observable<any>{
    return this.http.get<any>(this.url).
            pipe(
              filter(
                paiscanada => paiscanada['results'][0].location.country!= 'Canada'  
              )
            );
  }

  obtenerCantidadElementos():Observable<any>{
    return this.http.get<any>(this.url).
            pipe(
              take(1)
              );
  }

  obtenerFoto():Observable<any>{
    return this.http.get<any>(this.url).
    pipe(
      map(resp => {
      console.log(resp);
      return resp['results'].map((usuario: any) => {
       console.log(usuario);
      return {
       name: usuario.name,
      picture: usuario.picture
       }
      });
    })
   );
 }

 obtenerDatosPersonales():Observable<any>{
   return this.http.get<any>(this.url).
   pipe(
     map(respuesta => {
       console.log(respuesta);
       return respuesta['results'].map((usuario4: any) =>{
         console.log(usuario4);
       return {
         //usuario
         name: usuario4.name,
         //correo
         email: usuario4.email,
         //login
         login: usuario4.login,
         //fotos
         picture: usuario4.picture
       }
       })
       
     })
   )
 }

}

// nombre apellido 3 fotos correo nombre de usuario contraseña
      
