import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';
@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {
  usuario: any;
  usuario2: any;
  femenino: boolean;
  paiscanada: any;
  usuario3: any;

  usuario4: any;
 

  // nombre apellido 3 fotos correo nombre de usuario contraseña
  constructor(private servicioUsuario: UsuarioService) { 
    this.femenino = false;
    this.paiscanada = false;
  }
    
  ngOnInit(): void {
    this.servicioUsuario.obtenerUsuario().subscribe({
       next: user => { //next obtiene el valor que devuelve el observable
       this.usuario = user["results"][0]
       console.log(this.usuario);
     },
     error: error => { //error obtiene el error que devuelve el observable
       console.log(error);
     },
     complete: () => { //opcional
       console.log('solicitud completa');
       }
     })
      
  }

  showFemale(): void{
    this.servicioUsuario.obtenerUsuarioMasculino().subscribe({
    next: user => {
    //console.log(user);
     this.usuario2 = user['results'][0];
     this.femenino = true;
     },
     error: error => {
     console.log(error);
     }
    });
   }

  Pais():void{
    this.servicioUsuario.obtenerPaisCanada().subscribe({
      next: user => {
        this.usuario = user["results"][0];
        this.paiscanada = true;
        console.log(this.paiscanada);
        
      },
      error: error => {
        console.log(error);
      }
    })
  }
  MostrarElementos():void{
    this.servicioUsuario.obtenerCantidadElementos().subscribe({
      next: user => {
        console.log(user);
      },
      error: error => {
        console.log(error);
        
      }
    });
  }

  MostrarFoto(): void {
    this.servicioUsuario.obtenerFoto().subscribe({
    next: user => {
    console.log(user);
    this.usuario3 = user[0];
    },
    error: error => {
    console.log(error);
    }
  });
}
    
    MostrarDatosPersonales(): void{
      this.servicioUsuario.obtenerDatosPersonales().subscribe({
        next: user => {
          console.log(user);
          this.usuario4 = user[0];
        },
        error: error => {
          console.log(error);
          
        }
      })
    }

}





