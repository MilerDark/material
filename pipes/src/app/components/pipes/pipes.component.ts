import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.css']
})
export class PipesComponent implements OnInit {
  nombre: string = 'Ramiro Santos';
  nombre2: string = 'MaxiMiliAno PaReDes';
  array: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  PI: number = Math.PI;
  porcentaje: number = 0.234;
  salario: number = 1234.5;
  fecha: Date = new Date;


  heroe: any = {
    nombre: 'Logan',
    clave: 'Wolverine',
    edad: 500,
    direccion: {
      calle: 'Las vertientes #470',
      zona: 'Los Horizontes'
    }
  };

  valorPromesa = new Promise<string>((resolve) => {
    setTimeout(() =>{
      resolve('Llego la data');
    }, 3500);
  });
  
  constructor() { }

  ngOnInit(): void {
  }

}
