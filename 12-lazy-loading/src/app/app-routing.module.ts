import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';




const routes: Routes = [{
  //Si entra a auth carga el loadChildren
  path: 'auth',
  //Carga las rutas hijas, se cargan mediante el modulo, relacion a los componentes
  loadChildren: () =>import('./auth/auth.module').then(m => m.AuthModule),
},
{
  path: 'productos',
  loadChildren: () =>import('./productos/productos.module').then(m => m.ProductosModule),
},
{
  path: '**',
  redirectTo: 'auth'
}];

@NgModule({
  
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
