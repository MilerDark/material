import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
  @Input() ChildMessage!: string;
  @Output() EventoMensaje = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    console.log(this.ChildMessage);
    this.EventoMensaje.emit(this.mensaje);
    
  }

  mensaje = 'mensaje del hijo al padre';

}
