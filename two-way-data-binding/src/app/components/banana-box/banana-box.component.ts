import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-banana-box',
  templateUrl: './banana-box.component.html',
  styleUrls: ['./banana-box.component.css']
})
export class BananaBoxComponent implements OnInit {

  nombrePersona = '';
  constructor() { }

  ngOnInit(): void {
  }

  enviarMensaje(nombre:string){
    console.log(nombre);
    
  }
}
