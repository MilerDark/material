import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BananaBoxComponent } from './banana-box.component';

describe('BananaBoxComponent', () => {
  let component: BananaBoxComponent;
  let fixture: ComponentFixture<BananaBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BananaBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BananaBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
