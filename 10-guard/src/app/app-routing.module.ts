import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './components/about/about.component';
import { ColorComponent } from './components/color/color.component';
import { HomeComponent } from './components/home/home.component';
import { ColorGuard } from './guards/color.guard';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'color', component: ColorComponent, canActivate:[ColorGuard]},
  { path: 'about', component: AboutComponent},
  { path: '**', pathMatch: 'full', redirectTo:'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
