import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {

  @Input('appResaltado') nuevoColor!:string

  constructor(private el: ElementRef) { 
    console.log('Llamado de directiva');
    // el.nativeElement.style.backgroundColor = "yellow";
   }
   //Quiero escuchar en este caso al evento mouse enter
  @HostListener('mouseenter') mouseEnter(){
    // console.log(this.nuevoColor);
    this.resaltar(this.nuevoColor|| 'yellow');
    // this.el.nativeElement.style.backgroundColor = 'yellow'
  }

  //Quiero escuchar en este caso el evento mouse leave
  @HostListener('mouseleave') mouseSalio(){
    // this.el.nativeElement.style.backgroundColor = null;
    this.resaltar(null!)
  }

  private resaltar(color:string):void{
    this.el.nativeElement.style.backgroundColor = color;
  }
}
